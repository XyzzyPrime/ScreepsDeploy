module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-screeps');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.initConfig({
        secrets: grunt.file.readJSON('secrets.json'),
        clean: {
            contents: ["dest/"]
        },

        copy: {
            main: {
                expand: true,
                cwd: 'src/',
                src: '**/*',
                dest: 'dest/',
                filter: 'isFile',
                rename: function(dest, src) {
                    dest = dest + src.replace("/", ".");
                    return dest;
                }
            }
        },

        screeps: {
            options: {
                email: '<%= secrets.username %>',
                password: '<%= secrets.password %>',
                branch: '<%= secrets.branch %>',
                ptr: false
            },
            dist: {
                src: ['dest/*.js']
            }
        }
    });

    grunt.registerTask('deploy', ['clean', 'copy', 'screeps']);
}
